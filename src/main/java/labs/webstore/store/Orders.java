package labs.webstore.store;

import java.util.Iterator;
import java.util.LinkedList;

public class Orders <T extends  Order> {
    private LinkedList<T> list;

    Orders() {
        list = new LinkedList<>();
    }

    public LinkedList<T> getList() {
        return list;
    }

    public void add(T or) {
        list.add(or);
    }

    public void check() {
        Iterator iter = list.iterator();
        long time = System.currentTimeMillis();
        System.out.println("Время запуска обработки заказов " + time);
        while(iter.hasNext()) {
            T order = (T) iter.next();
            System.out.println("Заказ " + order.getPerson().getId());
            if(order.check_time(time)) {
                System.out.println("Заказ обработан ");
                iter.remove();
            }
            else
                System.out.println("Заказ не обработан ");
        }
    }

    public void delete(T or) { list.remove(or); }

    public void delete(int id) { list.remove(id); }

    public void view_elemensts() {
        if(list.isEmpty()) {
            System.out.println("Заказов нет");
        }
        else {
            Iterator iter = list.iterator();
            System.out.println("Заказы");
            while(iter.hasNext()) {
                T order = (T) iter.next();
                System.out.println(order.getPerson().getId() + " time_finish " + order.getFinish());
            }
        }
    }

    public Order getOrderId(int id) {
        for(Order el : list)
            if(el.getId() == id) return el;

        return null;
    }

    public int isOrderById(int orderId) {
        int i = 0;
        for(Order el : list) {
            if(el.getId() == orderId) return i;
            i++;
        }
        return -1;
    }
}

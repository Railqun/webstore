package labs.webstore.store;

import java.io.Serializable;
import java.util.Random;

public class Order implements Serializable {
    private static int number = 0;
    private int id;
    private OrderStatus status;
    private Credentials person;
    private ShopingCart basket;
    private long start;
    private long diff;
    private long finish;

    public void create_order(Credentials per, ShopingCart shop) {
        id = number++;
        status = OrderStatus.AWAITING;
        person = per;
        basket = shop;
        start = System.currentTimeMillis();

        Random rand = new Random();
        diff = 1000 + rand.nextInt(5000);
        finish = start + diff;
    }

    public boolean check_time(long data_check) {
        //System.out.println("time_1 \t\t" + start);
        //System.out.println("diff \t\t" + diff);
        //System.out.println("time_finish \t" + finish);

        if((start + diff) < data_check) {
            status = OrderStatus.PROCESSSED;
            return true;
        }
        else
            return false;
    }

    public void OrderStringStatus() {
        System.out.println(this.getId() + " time_finish " + this.getFinish()
                + " " + this.getStatus());
    }

    public OrderStatus getStatus() {
        return status;
    }

    public long getStart() {
        return start;
    }

    public long getDiff() {
        return diff;
    }

    public Credentials getPerson() {
        return person;
    }

    public long getFinish() {
        return finish;
    }

    public int getId() { return id; }

    public ShopingCart getBasket() {
        return basket;
    }

    enum OrderStatus {
        AWAITING, PROCESSSED
    }
}




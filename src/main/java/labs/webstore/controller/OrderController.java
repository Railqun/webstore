package labs.webstore.controller;

import labs.webstore.store.Generate;
import labs.webstore.store.Order;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class OrderController {
    private int counter = 4;

    private List<Map<String, String>> messages = new ArrayList<Map<String, String>>() {{
        add(new HashMap<String, String>() {{ put("id", "1"); put("text", "First message"); }});
        add(new HashMap<String, String>() {{ put("id", "2"); put("text", "Second message"); }});
        add(new HashMap<String, String>() {{ put("id", "3"); put("text", "Third message"); }});
    }};

    private static final Logger LOGGER = Logger.getLogger(OrderController.class.getSimpleName());

    @Autowired
    Generate gen;

    @GetMapping("command/readall")
    public List<Order> getReadAll() {
        LOGGER.info("command = readall, вывод всех заказов");
        return gen.getOrr().getList();
    }

    @GetMapping("command/readById")
    public Order getOrderById(@RequestParam(value = "order_id", required = true) String orderId) {
        LOGGER.info("command = readById, вывод заказа с order_id = " + orderId);

        Order order = gen.getOrr().getOrderId(Integer.parseInt(orderId));

        if(order != null) {
            return order;
        }
        else {
            LOGGER.warn("Нет заказа с orderId " + orderId);
            return null;
        }
    }

    @GetMapping("command/addToCard")
    public String addToCard(@RequestParam(value = "card_id", required = true) String cardId) {
        String str = "command = addToCard, добавление товара в корзину с cardId = " + cardId;

        LOGGER.info(str);
        gen.addClothes(Integer.parseInt(cardId));

        return str;
    }

    @GetMapping("command/delById")
    public String delById(@RequestParam(value= "order_id", required = true) String orderId) {
        String str = "command = delById, удаление заказа с order_id = " + orderId;

        LOGGER.info(str);

        int k = gen.delOrder(Integer.parseInt(orderId));

        if(k > 0) {
            str = "Нет заказа для удаления с orderId " + orderId;
        }
        else {
            str += "\n прошло успешно";
        }

        return str;
    }

    /*@GetMapping("order")
    public List<Order> list() {
        return gen.getOrr().getList();
    }

    @GetMapping("/")
    public List<Order> listTest(@RequestParam("command") String str,
                                @RequestParam(value = "order_id", required = false) String orderId,
                                @RequestParam(value = "card_id", required = false) String cardId) {
        //вывод всех строк
        if(str.equals("readall")) return gen.getOrr().getList();

        //возвращается заказ с указанным id
        if(str.equals("readById")) {
            List<Order> list = new ArrayList<>();
            list.add(gen.getOrr().getOrderId(Integer.parseInt(orderId) - 1));

            return list;
        }

        //генерация товара и добавление в корзину с идентификатором id(cardId)
        if(str.equals("addToCard")) {

        }

        return null;
    }*/

    /*@GetMapping("order/{id}")
    public Order getOne(@PathVariable String id) {
        return gen.getOrr().getOrderId(Integer.parseInt(id) - 1);
    }

    private Map<String, String> getMessage(@PathVariable String id) {
        return messages.stream()
                .filter(message -> message.get("id").equals(id))
                .findFirst()
                .orElseThrow(NotFoundException::new);
    }

    @PostMapping("order")
    public Map<String, String> create(@RequestBody Map<String, String> message) {
        message.put("id", String.valueOf(counter++));

        messages.add(message);

        return message;
    }

    @PutMapping("order/{id}")
    public Map<String, String> update(@PathVariable String id, @RequestBody Map<String, String> message) {
        Map<String, String> messageFromDb = getMessage(id);

        messageFromDb.putAll(message);
        messageFromDb.put("id", id);

        return messageFromDb;
    }

    @DeleteMapping("order/{id}")
    public void delete(@PathVariable String id) {
        Map<String, String> message = getMessage(id);

        messages.remove(message);
    }*/
}
